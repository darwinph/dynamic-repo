defmodule DynamicRepoWeb.Router do
  use DynamicRepoWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", DynamicRepoWeb do
    pipe_through :api
  end
end
