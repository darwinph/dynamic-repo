defmodule DynamicRepo do
  @moduledoc """
  DynamicRepo keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def all(pid, struct, opts \\ []) do
    Ecto.Repo.Queryable.all(pid, struct, opts)
  end

  def insert(pid, struct, opts \\ []) do
    Ecto.Repo.Schema.insert(pid, struct, opts)
  end

  def update(pid, struct, opts \\ []) do
    Ecto.Repo.Schema.update(pid, struct, opts)
  end

  def delete(pid, struct, opts \\ []) do
    Ecto.Repo.Schema.delete(pid, struct, opts)
  end
end

defmodule DynamicRepo.ChooseRepo do
  defstruct [:repo_id]
  alias __MODULE__

  def start(%ChooseRepo{repo_id: 1}) do
    {:ok, pid} = DynamicRepo.Repo.start_link(name: nil)
    pid
  end

  def start(%ChooseRepo{repo_id: 2}) do
    {:ok, pid} = DynamicRepo.AnotherRepo.start_link(name: nil)
    pid
  end
end
