defmodule DynamicRepo.Repo do
  use Ecto.Repo,
    otp_app: :dynamic_repo,
    adapter: Ecto.Adapters.Postgres
end

defmodule DynamicRepo.AnotherRepo do
  use Ecto.Repo,
    otp_app: :dynamic_repo,
    adapter: Ecto.Adapters.Postgres
end
