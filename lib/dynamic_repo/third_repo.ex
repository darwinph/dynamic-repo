defmodule DynamicRepo.ThirdRepo do
  use Ecto.Repo,
    otp_app: :dynamic_repo,
    adapter: Ecto.Adapters.Postgres
end
