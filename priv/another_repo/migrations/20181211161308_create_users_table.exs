defmodule DynamicRepo.AnotherRepo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add(:name, :string)
      add(:age, :integer)

      timestamps()
    end
  end
end
